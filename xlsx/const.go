package xlsx

const (
	_XL               = "xl/"
	_SharedStringPath = "xl/sharedStrings.xml"
	_StylesPath       = "xl/styles.xml"
	_WorkBookPath     = "xl/workbook.xml"
	_WorkBookRelsPath = "xl/_rels/workbook.xml.rels"

	_AllNumber = "0123456789"

	// xml
	_Worksheet   = "worksheet"
	_SheetData   = "sheetData"
	_Row         = "row"
	_S           = "s"
	_SI          = "si"
	_T           = "t"
	_R           = "r"
	_SST         = "sst"
	_Count       = "count"
	_UniqueCount = "uniqueCount"
	_C           = "c"
	_V           = "v"

	_RelTypeWorkSheet = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet"
)
