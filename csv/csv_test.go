package csv_test

import (
	"errors"
	"io"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-spreadsheet/csv"
)

var testCases = []struct {
	FileName string
	Result   [][]any
}{
	{
		FileName: "ms-100k.csv",
		Result:   nil,
	},
	{
		FileName: "valid.csv",
		Result: [][]any{
			{
				"AlphaName",
				"79310000019",
				"Hello, World!",
				"02.04.2023 18:00",
				"Батомонкуев Дондок Бороевич",
				"ПАО Оманид",
				"Mail.RU",
			},
			{
				"",
				"",
				"Hello, World!",
				"06.04.2023 18:00",
				"Батомонкуев Дондок Бороевич",
				"ООО Табеан",
				"Facebook",
			},
			{
				"AlphaName",
				"79310000025",
				"Test message",
				"08.04.2023 18:00",
				"Батомонкуев Дондок Бороевич",
				"Вектор",
				"Mail.RU",
			},
		},
	},
}

func TestOpenBinary(t *testing.T) {
	for _, test := range testCases {
		t.Run(test.FileName, func(t *testing.T) {
			start := time.Now()

			bytes, err := os.ReadFile("../test/csv/" + test.FileName)
			require.NoError(t, err)

			doc, err := csv.OpenBinary(bytes)
			require.NoError(t, err)

			res := make([][]any, 0, 100)

			for {
				row, err := doc.Row()
				if errors.Is(err, io.EOF) {
					break
				}

				require.NoError(t, err)

				res = append(res, row)
			}

			t.Logf("%v", time.Since(start))

			if test.Result != nil {
				require.Equal(t, test.Result, res)
			}
		})
	}
}

func TestOpenFile(t *testing.T) {
	for _, test := range testCases {
		t.Run(test.FileName, func(t *testing.T) {
			start := time.Now()

			doc, err := csv.OpenFile("../test/csv/" + test.FileName)
			require.NoError(t, err)
			defer doc.Close()

			res := make([][]any, 0, 100)

			for {
				row, err := doc.Row()
				if errors.Is(err, io.EOF) {
					break
				}

				require.NoError(t, err)

				res = append(res, row)
			}

			t.Logf("%v", time.Since(start))

			if test.Result != nil {
				require.Equal(t, test.Result, res)
			}
		})
	}
}
