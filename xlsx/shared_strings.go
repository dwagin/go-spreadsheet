package xlsx

import (
	"archive/zip"
	"encoding/xml"
	"strconv"
)

func parseSharedStrings(sharedStringFile *zip.File) ([]string, error) {
	if sharedStringFile == nil {
		return nil, nil
	}

	decoder, closer, err := openXML(sharedStringFile)
	if err != nil {
		return nil, err
	}
	defer closer.Close()

	var (
		token  xml.Token
		shared []string
	)

	tStart := false
	value := make([]byte, 0, 1024)

	for {
		if token, err = decoder.Token(); err != nil {
			return nil, err
		}

		switch token := token.(type) {
		case xml.StartElement:
			switch token.Name.Local {
			case _SI:
			case _T:
				tStart = true
			case _SST:
				count := 0
				unqCount := 0
				for _, attr := range token.Attr {
					switch attr.Name.Local {
					case _Count:
						if count, err = strconv.Atoi(attr.Value); err != nil {
							return nil, err
						}
					case _UniqueCount:
						if unqCount, err = strconv.Atoi(attr.Value); err != nil {
							return nil, err
						}
					}
				}
				if unqCount != 0 {
					shared = make([]string, 0, unqCount)
				} else {
					shared = make([]string, 0, count)
				}
			}
		case xml.EndElement:
			switch token.Name.Local {
			case _SI:
				shared = append(shared, string(value))
				value = value[:0]
			case _SST:
				return shared, nil
			}
		case xml.CharData:
			if tStart {
				value = append(value, token...)
				tStart = false
			}
		}
	}
}
