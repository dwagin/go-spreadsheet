package csv

import (
	"bytes"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
)

type CSV struct {
	reader *csv.Reader
}

type ReadCloser struct {
	CSV
	closer io.ReadCloser
}

func (v *ReadCloser) Close() error {
	err := v.closer.Close()
	if err != nil {
		return fmt.Errorf("close file error: %v", err)
	}

	return nil
}

func OpenBinary(b []byte, comma ...rune) (*CSV, error) {
	reader := csv.NewReader(bytes.NewReader(b))
	if len(comma) > 0 {
		reader.Comma = comma[0]
	}

	reader.FieldsPerRecord = -1
	reader.ReuseRecord = true

	return &CSV{reader: reader}, nil
}

func OpenFile(filename string, comma ...rune) (*ReadCloser, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("open file error: %v", err)
	}

	reader := csv.NewReader(file)
	if len(comma) > 0 {
		reader.Comma = comma[0]
	}

	reader.FieldsPerRecord = -1
	reader.ReuseRecord = true

	return &ReadCloser{
		CSV:    CSV{reader: reader},
		closer: file,
	}, nil
}

func (v *CSV) Row() ([]any, error) {
	record, err := v.reader.Read()
	if err != nil {
		if errors.Is(err, io.EOF) {
			return nil, fmt.Errorf("no more data: %w", io.EOF)
		}

		return nil, fmt.Errorf("read row error: %v", err)
	}

	row := make([]any, len(record))
	for idx, val := range record {
		row[idx] = val
	}

	return row, nil
}
