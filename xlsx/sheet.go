package xlsx

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"strconv"
	"time"
)

type Sheet struct {
	xlsx    *XLSX
	decoder *xml.Decoder
	closer  io.Closer
}

func (v *XLSX) Sheet(name string) (*Sheet, error) {
	file, ok := v.sheetFiles[name]
	if !ok {
		return nil, fmt.Errorf("worksheet %q not exist", name)
	}

	decoder, closer, err := openXML(file)
	if err != nil {
		return nil, err
	}

	err = skipToXML(decoder, _Worksheet)
	if err == nil {
		err = skipToXML(decoder, _SheetData)
	}

	if err != nil {
		if errors.Is(err, io.EOF) {
			return nil, fmt.Errorf("empty worksheet %q", name)
		}
		return nil, fmt.Errorf("unable to parse worksheet %q: %v", name, err)
	}

	return &Sheet{
		xlsx:    v,
		decoder: decoder,
		closer:  closer,
	}, nil
}

func (v *Sheet) nextRow() error {
	var (
		err   error
		token xml.Token
	)

	for {
		if token, err = v.decoder.Token(); err != nil {
			return err
		}

		switch token := token.(type) {
		case xml.StartElement:
			if token.Name.Local == _Row {
				return nil
			}
		case xml.EndElement:
			if token.Name.Local == _SheetData {
				return io.EOF
			}
		}
	}
}

func (v *Sheet) Row() ([]any, error) {
	var (
		err   error
		token xml.Token
	)

	if err = v.nextRow(); err != nil {
		return nil, err
	}

	row := make([]any, 0, 10)

	cellType := BlankCellType
	cellStyleID := -1
	cellValue := make([]byte, 0, 1024)

	tStart, vStart := false, false
	for {
		if token, err = v.decoder.Token(); err != nil {
			return nil, err
		}

		switch token := token.(type) {
		case xml.StartElement:
			switch token.Name.Local {
			case _C:
				cellType = BlankCellType
				cellStyleID = -1
				for _, a := range token.Attr {
					switch a.Name.Local {
					case _T:
						cellType = CellType(a.Value)
					case _S:
						cellStyleID, err = strconv.Atoi(a.Value)
						if err != nil {
							return nil, err
						}
					}
				}
			case _T:
				tStart = true
			case _V:
				vStart = true
				if cellType == BlankCellType {
					cellType = NumberCellType
				}
			}
		case xml.EndElement:
			switch token.Name.Local {
			case _C:
				var val any
				switch cellType {
				case BlankCellType:
				case BooleanCellType:
					if len(cellValue) > 0 && cellValue[0] == '1' {
						val = true
					} else {
						val = false
					}
				case DateCellType:
					var t time.Time
					t, err = time.Parse(time.RFC3339Nano, string(cellValue))
					if err != nil {
						return nil, err
					}
					val = t
				case NumberCellType:
					var num float64
					num, err = strconv.ParseFloat(string(cellValue), 64)
					if err != nil {
						return nil, err
					}
					if cellStyleID >= 0 && v.xlsx.dateStyles[cellStyleID] {
						val = parseExcelTime(num)
					} else {
						val = num
					}
				case SharedStringCellType:
					var idx int
					idx, err = strconv.Atoi(string(cellValue))
					if err != nil {
						return nil, err
					}
					if idx < 0 || idx >= len(v.xlsx.sharedStrings) {
						return nil, fmt.Errorf("shared string idx=%d out of range len=%d", idx, len(v.xlsx.sharedStrings))
					}
					val = v.xlsx.sharedStrings[idx]
				case ErrorCellType, FormulaStringCellType, InlineStringCellType:
					val = string(cellValue)
				}
				row = append(row, val)
				cellValue = cellValue[:0]
			case _Row:
				return row, nil
			}
		case xml.CharData:
			if vStart || tStart {
				cellValue = append(cellValue, token...)
				vStart = false
				tStart = false
			}
		}
	}
}

func (v *Sheet) Close() error {
	return v.closer.Close()
}
