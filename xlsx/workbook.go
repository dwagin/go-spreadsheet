package xlsx

import (
	"archive/zip"
	"fmt"
	"strings"
)

type workbookRels struct {
	Relationships []workbookRelation `xml:"Relationship"`
}

type workbookRelation struct {
	ID     string `xml:"Id,attr"`
	Target string `xml:"Target,attr"`
	Type   string `xml:"Type,attr"`
}

type workbook struct {
	Sheets []sheet `xml:"sheets>sheet"`
}

type sheet struct {
	Name           string `xml:"name,attr,omitempty"`
	RelationshipID string `xml:"http://schemas.openxmlformats.org/officeDocument/2006/relationships id,attr,omitempty"`
}

func parseWorkbook(workbookRelsFile *zip.File, workbookFile *zip.File, files []*zip.File) ([]string, map[string]*zip.File, error) {
	wb := workbook{}
	wbRels := workbookRels{}

	decoder, closer, err := openXML(workbookFile)
	if err != nil {
		return nil, nil, err
	}
	err = decoder.Decode(&wb)
	closer.Close()
	if err != nil {
		return nil, nil, err
	}

	decoder, closer, err = openXML(workbookRelsFile)
	if err != nil {
		return nil, nil, err
	}
	err = decoder.Decode(&wbRels)
	closer.Close()
	if err != nil {
		return nil, nil, err
	}

	sheetFiles := make(map[string]*zip.File, len(wb.Sheets))
	sheetNames := make([]string, len(wb.Sheets))

	for i, s := range wb.Sheets {
		var sheetFilename string
		for _, rel := range wbRels.Relationships {
			if rel.Type == _RelTypeWorkSheet && rel.ID == s.RelationshipID {
				if strings.HasPrefix(rel.Target, "/") {
					// path is absolute, take all but the leading slash
					sheetFilename = rel.Target[1:]
				} else {
					sheetFilename = _XL + rel.Target
				}
				break
			}
		}

		if sheetFilename == "" {
			return nil, nil, fmt.Errorf("unable to find file with relationship %s", s.RelationshipID)
		}

		var sheetFile *zip.File
		for _, file := range files {
			if file.Name == sheetFilename {
				sheetFile = file
				break
			}
		}

		if sheetFile == nil {
			return nil, nil, fmt.Errorf("unable to get file for sheet name %s", sheetFilename)
		}

		sheetNames[i] = s.Name
		sheetFiles[s.Name] = sheetFile
	}

	return sheetNames, sheetFiles, nil
}
