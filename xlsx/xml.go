package xlsx

import (
	"archive/zip"
	"encoding/xml"
	"io"
)

func openXML(file *zip.File) (*xml.Decoder, io.Closer, error) {
	closer, err := file.Open()
	if err != nil {
		return nil, nil, err
	}
	decoder := xml.NewDecoder(closer)
	return decoder, closer, nil
}

func skipToXML(decoder *xml.Decoder, tokenName string) error {
	var (
		err   error
		token xml.Token
	)

	for {
		if token, err = decoder.Token(); err != nil {
			return err
		}

		switch token := token.(type) {
		case xml.StartElement:
			switch token.Name.Local {
			case tokenName:
				return nil
			default:
				if err = decoder.Skip(); err != nil {
					return err
				}
			}
		}
	}
}
