#!/bin/sh

find . -mindepth 1 -maxdepth 5 -type d | sort | egrep -v '(.git|.idea|vendor)' | \
while read DIR; do
	echo "${DIR}"
	cd "${DIR}" && go get -u && go mod tidy && go mod vendor
	cd -
done

git add -A vendor
