package xlsx

import (
	"math"
	"time"
)

const (
	oneDay       = float64(24 * time.Hour)
	roundEpsilon = 1e-9
)

var excelEpoch = time.Date(1899, 12, 30, 0, 0, 0, 0, time.UTC)

func parseExcelTime(value float64) time.Time {
	days := math.Trunc(value)
	part := time.Duration((value - days + roundEpsilon) * oneDay)
	return excelEpoch.AddDate(0, 0, int(days)).Add(part)
}
