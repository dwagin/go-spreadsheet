package xlsx

import (
	"archive/zip"
	"regexp"
	"strings"
)

type styleSheet struct {
	NumberFormats []numberFormat `xml:"numFmts>numFmt"`
	CellStyles    []cellStyle    `xml:"cellXfs>xf"`
}

type numberFormat struct {
	NumberFormatID int    `xml:"numFmtId,attr"`
	FormatCode     string `xml:"formatCode,attr"`
}

type cellStyle struct {
	NumberFormatID int `xml:"numFmtId,attr"`
}

func parseStyles(stylesFile *zip.File) (map[int]bool, error) {
	ss := styleSheet{}

	decoder, closer, err := openXML(stylesFile)
	if err != nil {
		return nil, err
	}
	err = decoder.Decode(&ss)
	closer.Close()
	if err != nil {
		return nil, err
	}

	dateStyles := map[int]bool{}

	for idx, style := range ss.CellStyles {
		if style.NumberFormatID >= 14 && style.NumberFormatID <= 22 {
			dateStyles[idx] = true
		}
		if style.NumberFormatID >= 164 {
			code := formatCode(style.NumberFormatID, ss.NumberFormats)
			if isDateFormatCode(code) {
				dateStyles[idx] = true
			}
		}
	}

	if len(dateStyles) > 0 {
		return dateStyles, nil
	}

	return nil, nil
}

func formatCode(ID int, numberFormats []numberFormat) string {
	for _, nf := range numberFormats {
		if nf.NumberFormatID == ID {
			return nf.FormatCode
		}
	}

	return ""
}

var formatGroup = regexp.MustCompile(`\[.+?\]|\\.|".*?"`)

func isDateFormatCode(code string) bool {
	c := formatGroup.ReplaceAllString(code, "")
	return strings.ContainsAny(c, "dmhysDMHYS")
}
